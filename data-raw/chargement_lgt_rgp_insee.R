# logements et motorisation des ménages recensement INSEE

# rm(list=ls())

## Chargement des packages ####

library(dplyr)
library(tidyr)
library(purrr)
library(lubridate)
library(readxl)
library(xlsx)
library(readODS)
library(forcats)

# chargement des données logement du dernier recensement INSEE
# doc INSEE disponible à https://www.insee.fr/fr/statistiques/4515532?sommaire=4516107


## Paramètres à saisir ####
url <- "https://www.insee.fr/fr/statistiques/fichier/4515532/base-ccx-logement-2017.zip"
mil <- c(2017, 2012, 2007) # mettre le millesime le plus récent en premier
fic <- "base-cc-logement-2017"
# prefixe des variables à enlever (indique le millésime)
var_mil <- paste0("P", mil,"_") %>% gsub("20", "", .) 
var_mil_comp <- paste0("C", mil,"_") %>% gsub("20", "", .)


## Téléchargement, dézipage 

download.file(url, destfile = "extdata/lgt_RGP_INSEE.zip")
unzip("extdata/lgt_RGP_INSEE.zip", exdir="extdata")


## lecture #########

lgt_rp_actu <- read_xlsx(path = paste0("extdata/", fic,".xlsx"), sheet =paste0("COM_", mil[1]), skip = 5) %>%
  mutate(annee=as.character(mil[1])) %>%
  rename_at(vars(starts_with(var_mil[1])), funs(gsub(var_mil[1], "", .))) %>%
  rename_at(vars(starts_with(var_mil_comp[1])), funs(sub(var_mil_comp[1], "", .)))

lgt_rp_old <-  read_xlsx(path = paste0("extdata/", fic,".xlsx"), sheet =paste0("COM_", mil[2]), skip = 5) %>%
  mutate(annee=as.character(mil[2])) %>%
  rename_at(vars(starts_with(var_mil[2])), funs(gsub(var_mil[2], "", .))) %>%
  rename_at(vars(starts_with(var_mil_comp[2])), funs(sub(var_mil_comp[2], "", .)))

lgt_rp_very_old <-  read_xlsx(path = paste0("extdata/", fic,".xlsx"), sheet = paste0("COM_", mil[3]), skip = 5) %>%
  mutate(annee=as.character(mil[3])) %>%
  rename_at(vars(starts_with(var_mil[3])), funs(gsub(var_mil[3], "", .)))  %>%
  rename_at(vars(starts_with(var_mil_comp[3])), funs(sub(var_mil_comp[3], "", .)))

# renomage des libellés de variables grace à une table de passage construite sous calc
lib_var <- read_ods(path = "data/metadata/Insee_RGP_lgt_lib_variable.ods", col_types = NA) %>%
  mutate_all(as.factor) %>%
  mutate(a_supprimer=as.logical(a_supprimer)) %>%
  select(a_supprimer:variable)

lgt_rp <- bind_rows(lgt_rp_actu, lgt_rp_old, lgt_rp_very_old) %>%
  select(date=annee, depcom=CODGEO, everything(), -(REG:LIBGEO)) %>%
  gather(key=variable_deb, value=valeur, -date, -depcom) %>%
  left_join(lib_var) %>%  # filter(lgt_rp, is.na(variable)) %>% select(variable_deb) %>% distinct() pour vérifier qu'il n'y a pas de nouvelles variables par rapport aux millésimes précédents
  filter(!a_supprimer) %>%
  select(-variable_deb, -a_supprimer) %>%
  mutate(date=make_date(date, 12, 31)) %>%
  mutate_if(is_character, as.factor) %>%
  mutate_if(is.factor, fct_drop)

# on enlève les lignes communales Paris Lyon et Marseille si elles doublonnent les lignes d'arrondissement
if(all(c("75056","75101") %in% lgt_rp$depcom)) {
  pop <-  filter(lgt_rp, !(depcom %in% c("75056", "13055", "69123")))
}

save(lgt_rp, file="sysdata/chargement_lgt_rgp_insee.RData")

rm(lgt_rp_actu, lgt_rp_old, lgt_rp_very_old, fic, mil, url, lib_var, var_mil, var_mil_comp)
