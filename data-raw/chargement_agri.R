
# librairies -------
library(readxl)
library(tidyverse)
library(lubridate)

# library(readr)

rm(list = ls())

# données Agreste/recensement agricole 2010 ----------------
# site Agreste recensement agricole 2010 (http://agreste.agriculture.gouv.fr/recensement-agricole-2010/resultats-donnees-chiffrees/)
# fichier  « Principaux   résultats par commune (Zip : 4.4 Mo) »
# Les principaux résultats chiffrés au format tableur des recensements agricoles 2010, 2000 et 1988 
# par département, canton et commune.
# Nombre d’exploitations, travail (en unité de travail annuel), superficie agricole utilisée (SAU), 
# superficie en terres labourables, en cultures permanentes, superficie toujours en herbe,
# Données sur le cheptel, les cultures et la main-d’oeuvre.
# Les données sont localisées à la commune du siège de l’exploitation
# En particulier, la SAU est celle des exploitations ayant leur siège dans la commune et non celle de la commune. 
agreste_ra_donnees_principales_commune<-read_excel(path = "DONNEES_CLIENT/DRAAF/agreste_ra_donnees_principales_commune.xls")

agreste_ra<-agreste_ra_donnees_principales_commune %>%
  mutate(`exploitation 2010` = as.numeric(`exploitation 2010`),
         `uta 2010` = as.numeric(`uta 2010`),
         `uta 2000` = as.numeric(`uta 2000`),
         `uta 1988` = as.numeric(`uta 1988`),
         `sau 2010` = as.numeric(`sau 2010`),
         `sau 2000` = as.numeric(`sau 2000`),
         `sau 1988` = as.numeric(`sau 1988`),
         `ugbta 2010` = as.numeric(`ugbta 2010`),
         `ugbta 2000` = as.numeric(`ugbta 2000`),
         `ugbta 1988` = as.numeric(`ugbta 1988`),
         `stl 2010` = as.numeric(`stl 2010`),
         `stl 2000` = as.numeric(`stl 2000`),
         `stl 1988` = as.numeric(`stl 1988`),
         `scp 2010` = as.numeric(`scp 2010`),
         `scp 2000` = as.numeric(`scp 2000`),
         `scp 1988` = as.numeric(`scp 1988`),
         `sth 2010` = as.numeric(`sth 2010`),
         `sth 2000` = as.numeric(`sth 2000`),
         `sth 1988` = as.numeric(`sth 1988`)) %>% 
  mutate_if(is.character,as.factor) %>% 
  select(-Modification)

exploitation<-agreste_ra %>% 
  select(`Code géographique`:Département,starts_with("exploitation")) %>%
  gather(key = "variable",value = "valeur",`exploitation 2010`:`exploitation 1988`) %>%
  mutate(date = as.numeric(str_sub(variable,14,17))) %>%
  mutate(date = dmy(paste("01-01",date,sep = "-"))) %>% 
  mutate(variable = "exploitation") %>% 
  mutate_if(is.character,as.factor)

uta<-agreste_ra %>%
  select(`Code géographique`:Département,starts_with("uta")) %>%
  gather(key = "variable",value = "valeur",`uta 2010`:`uta 1988`) %>%
  mutate(date = as.numeric(str_sub(variable,5,8))) %>%
  mutate(date = dmy(paste("01-01",date,sep = "-"))) %>% 
  mutate(variable = "unite_travail_annuel") %>% 
  mutate_if(is.character,as.factor)

sau<-agreste_ra %>% 
  select(`Code géographique`:Département,starts_with("sau")) %>%
  gather(key = "variable",value = "valeur",`sau 2010`:`sau 1988`) %>%
  mutate(date = as.numeric(str_sub(variable,5,8))) %>%
  mutate(date = dmy(paste("01-01",date,sep = "-"))) %>% 
  mutate(variable = "superficie_agricole_utilisee_siege") %>% 
  mutate_if(is.character,as.factor)

ugbta<-agreste_ra %>% 
  select(`Code géographique`:Département,starts_with("ugbta")) %>%
  gather(key = "variable",value = "valeur",`ugbta 2010`:`ugbta 1988`) %>%
  mutate(date = as.numeric(str_sub(variable,7,10))) %>%
  mutate(date = dmy(paste("01-01",date,sep = "-"))) %>% 
  mutate(variable = "unite_gros_betail_tous_aliments") %>% 
  mutate_if(is.character,as.factor)

stl<-agreste_ra %>% 
  select(`Code géographique`:Département,starts_with("stl")) %>%
  gather(key = "variable",value = "valeur",`stl 2010`:`stl 1988`) %>%
  mutate(date = as.numeric(str_sub(variable,5,8))) %>%
  mutate(date = dmy(paste("01-01",date,sep = "-"))) %>% 
  mutate(variable = "superficie_terre_labourable") %>% 
  mutate_if(is.character,as.factor)

scp<-agreste_ra %>% 
  select(`Code géographique`:Département,starts_with("scp")) %>%
  gather(key = "variable",value = "valeur",`scp 2010`:`scp 1988`) %>%
  mutate(date = as.numeric(str_sub(variable,5,8))) %>%
  mutate(date = dmy(paste("01-01",date,sep = "-"))) %>% 
  mutate(variable = "superficie_culture_permanente") %>% 
  mutate_if(is.character,as.factor)

sth<-agreste_ra %>% 
  select(`Code géographique`:Département,starts_with("sth")) %>%
  gather(key = "variable",value = "valeur",`sth 2010`:`sth 1988`) %>%
  mutate(date = as.numeric(str_sub(variable,5,8))) %>%
  mutate(date = dmy(paste("01-01",date,sep = "-"))) %>% 
  mutate(variable = "superficie_toujours_en_herbe") %>% 
  mutate_if(is.character,as.factor)

agri<-bind_rows(exploitation,uta,sau,ugbta,stl,scp,sth) %>% 
  mutate_if(is.character,as.factor) %>% 
  mutate(valeur = ifelse(is.na(valeur),0,valeur)) %>% 
  rename(depcom = `Code géographique`) %>% 
  select(-c(`Libellé de commune`,`Niveau géographique`,Région,Département))%>% 
  complete(depcom,date,variable,fill = list(valeur =0))

rm(exploitation,uta,sau,ugbta,stl,scp,sth)
rm(agreste_ra_donnees_principales_commune,agreste_ra)


# sauvegarde -----------------
save.image(file = "sysdata/chargement_agri.RData")
