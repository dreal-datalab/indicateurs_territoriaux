
# librairies ----------
# library(RPostgreSQL)
library(dplyr)
library(tidyverse)
library(glue)
library(sf)
library(mefa4)
library(mapview)
library(stringr)
library(rgdal)
library(readxl)
library(COGiter)

rm(list=ls())


# connexion au SGBDR ------------
dsn_production<-glue("PG:host={Sys.getenv('server')} port={Sys.getenv('port')} dbname='production' user='does' password={Sys.getenv('pwd_does')}")
dsn_si_eau<-glue("PG:host={Sys.getenv('server')} port={Sys.getenv('port')} dbname='si_eau' user='dreal' password={Sys.getenv('pwd_dreal')}")
dsn_referentiels<-glue("PG:host={Sys.getenv('server')} port={Sys.getenv('port')} dbname='referentiels' user='dreal' password={Sys.getenv('pwd_dreal')}")
dsn_consultation<-glue("PG:host={Sys.getenv('server')} port={Sys.getenv('port')} dbname='consultation' user='dreal' password={Sys.getenv('pwd_dreal')}")
# dsn_datamart<-glue("PG:host={Sys.getenv('server')} port={Sys.getenv('port')} dbname='datamart' user='dreal' password={Sys.getenv('pwd_dreal')}")


# limites administratives ----------
n_region_exp_r52<-st_read(dsn_referentiels,query = "SELECT * FROM adminexpress.n_region_exp_r52")
n_region_exp_r52<-n_region_exp_r52 %>% 
  select(insee_reg,nom_reg) %>% 
  mutate(surf_region=st_area(the_geom))
n_departement_exp_r52<-st_read(dsn_referentiels,query = "SELECT * FROM adminexpress.n_departement_exp_r52")
n_departement_exp_r52<-n_departement_exp_r52 %>% 
  select(insee_dep,nom_dep) %>% 
  mutate(surf_departement=st_area(the_geom))
n_commune_exp_r52<-st_read(dsn_referentiels,query = "SELECT * FROM adminexpress.n_commune_exp_r52")
n_commune_exp_r52<-n_commune_exp_r52 %>% 
  select(insee_com,nom_com,code_epci) %>% 
  mutate(surf_commune=st_area(the_geom))
n_epci_zsup_r52<-st_read(dsn_consultation,query = "SELECT * FROM donnee_generique.n_epci_zsup_r52")
n_epci_zsup_r52<-n_epci_zsup_r52 %>% 
  select(siren_epci,dep_epci,nom_epci) %>% 
  mutate(surf_epci=st_area(the_geom))


# vérification adminexpress et COGiter ------------
communes_pdl<-communes %>% 
  filter(REG== '52') %>% 
  select(DEPCOM,NOM_DEPCOM,EPCI) # 1238 obs
epci_pdl<-epci %>% 
  unnest(REGIONS_DE_L_EPCI) %>% 
  filter(REGIONS_DE_L_EPCI == '52') # 71 obs

x<-semi_join(n_commune_exp_r52,communes_pdl,by=c("insee_com"="DEPCOM","code_epci"="EPCI")) # 1238 obs
y<-semi_join(n_epci_zsup_r52,epci_pdl,by=c("siren_epci"="EPCI")) # 71 obs
rm(x,y)
# çà correspond

communes_pdl<-communes_pdl %>% 
  select(DEPCOM,NOM_DEPCOM) %>% 
  rename(CodeZone=DEPCOM,Zone=NOM_DEPCOM)

epci_pdl<-epci_pdl %>% 
  select(EPCI,NOM_EPCI) %>% 
  rename(CodeZone=EPCI,Zone=NOM_EPCI)

departements_pdl<-departements %>% 
  filter(REG=='52') %>% 
  select(DEP,NOM_DEP) %>% 
  rename(CodeZone=DEP,Zone=NOM_DEP)

region_pdl<-regions %>% 
  filter(REG=='52') %>% 
  select(REG,NOM_REG) %>% 
  rename(CodeZone=REG,Zone=NOM_REG)

territoire_pdl<-bind_rows(communes_pdl,
                          departements_pdl,
                          epci_pdl,
                          region_pdl) %>% 
  mutate_if(is.factor,as.character)

# save(communes_pdl,epci_pdl,departements_pdl,region_pdl,file = "sysdata/verif_territoires_cogiter.RData")


# stations, captages eau potable, captages prioritaires ------------
station<-st_read(dsn_si_eau,query = "SELECT * FROM stations.station")
# station_sans_geom<-station %>% st_drop_geometry()
# rm(station_sans_geom)

captage_eau_potable_ars<-st_read(dsn_si_eau,query = "SELECT * FROM stations.captage_eau_potable_ars")
captage_eau_potable_ars<-right_join(station %>%
                                      select(code_station),
                                    captage_eau_potable_ars,
                                    by=c("code_station"="ins_code_national"))

station_captage_prioritaire<-st_read(dsn_production,query = "SELECT * FROM srnp_dema_si_eau.station_captage_prioritaire")
station_captage_prioritaire<-mutate(station_captage_prioritaire,Code_sta_1=as.character(Code_sta_1))
station_captage_prioritaire$Code_sta_1<- str_pad(station_captage_prioritaire$Code_sta_1,width=9,pad = "0",side = "left")
station_captage_prioritaire<-station_captage_prioritaire %>% 
  select(Code_sta_1,Prioritair) %>% 
  rename(code_station=Code_sta_1,liste_annees_prioritaire=Prioritair)

# verif<-semi_join(station_captage_prioritaire %>%
#                    select(code_station) %>% 
#                    st_drop_geometry(),
#                  captage_eau_potable_ars %>%
#                    select(code_station) %>%
#                    st_drop_geometry()) # 47 obs : c'est bon


# captages prioritaires et plan d'actions ---------
# selon srnp_dema courriel du 04/02/2021, les captages prioritaires ne possédant pas de Plan d'Actions sont :
# pour le 49 : la Rucette (049000394) ;le Longeron ;le Prieuré de la Madeleine (049000211) ;le Puits de la Fontaine Bourreau (049000479) ;
# pour le 85 : Sainte Germaine (085000184)

# le longeron est une ancienne commune, maintenant Sèvremoine
# en fait Barrage des rivières (049000402) http://www.maine-et-loire.gouv.fr/les-captages-prioritaires-grenelle-a6180.html

station_captage_prioritaire<-left_join(station_captage_prioritaire,
                                         station %>%
                                          st_drop_geometry() %>% 
                                          select(code_station,libelle_station)) %>% 
  mutate(plan_action=ifelse(code_station %notin% c("049000394","049000402","049000211","049000479","085000184"),"oui","non"))


# intersection captages prioritaires et territoires ---------                                       
intersection_captage_prioritaire_commune<-st_intersection(station_captage_prioritaire,st_buffer(n_commune_exp_r52,0))
# mapview(intersection_captage_prioritaire_commune,zcol=c("insee_com"),legend=F)+mapview(n_commune_exp_r52,alpha.regions=0)
intersection_captage_prioritaire_commune_2<-intersection_captage_prioritaire_commune %>% 
  st_drop_geometry() %>% 
  group_by(insee_com) %>% 
  mutate(nbre_captages_prioritaires=n()) %>% 
  ungroup() %>%
  select(insee_com,nbre_captages_prioritaires) %>% 
  unique() %>% 
  right_join(n_commune_exp_r52 %>% 
               select(insee_com) %>%
               st_drop_geometry()) %>% 
  mutate(nbre_captages_prioritaires=ifelse(is.na(nbre_captages_prioritaires),0,nbre_captages_prioritaires)) %>% 
  mutate(TypeZone="Communes",variable="nbre_captages_prioritaires",date=as.Date("2021-01-01")) %>% 
  rename(CodeZone=insee_com,valeur=nbre_captages_prioritaires)

intersection_captage_prioritaire_epci<-st_intersection(station_captage_prioritaire,st_buffer(n_epci_zsup_r52,0))
# mapview(intersection_captage_prioritaire_epci,zcol=c("siren_epci"),legend=F)+mapview(n_epci_zsup_r52,alpha.regions=0)
intersection_captage_prioritaire_epci_2<-intersection_captage_prioritaire_epci %>% 
  st_drop_geometry() %>% 
  group_by(siren_epci) %>% 
  mutate(nbre_captages_prioritaires=n()) %>% 
  ungroup() %>%
  select(siren_epci,nbre_captages_prioritaires) %>% 
  unique() %>% 
  right_join(n_epci_zsup_r52 %>% 
               select(siren_epci) %>%
               st_drop_geometry()) %>% 
  mutate(nbre_captages_prioritaires=ifelse(is.na(nbre_captages_prioritaires),0,nbre_captages_prioritaires)) %>% 
  mutate(TypeZone="Epci",variable="nbre_captages_prioritaires",date=as.Date("2021-01-01")) %>% 
  rename(CodeZone=siren_epci,valeur=nbre_captages_prioritaires)

intersection_captage_prioritaire_departement<-st_intersection(station_captage_prioritaire,st_buffer(n_departement_exp_r52,0))
# mapview(intersection_captage_prioritaire_departement,zcol=c("insee_dep"),legend=F)+mapview(n_departement_exp_r52,alpha.regions=0)
intersection_captage_prioritaire_departement_2<-intersection_captage_prioritaire_departement %>% 
  st_drop_geometry() %>% 
  group_by(insee_dep) %>% 
  mutate(nbre_captages_prioritaires=n()) %>% 
  ungroup() %>%
  select(insee_dep,nbre_captages_prioritaires) %>% 
  unique() %>% 
  right_join(n_departement_exp_r52 %>% 
               select(insee_dep) %>%
               st_drop_geometry()) %>% 
  mutate(nbre_captages_prioritaires=ifelse(is.na(nbre_captages_prioritaires),0,nbre_captages_prioritaires)) %>% 
  mutate(TypeZone="Départements",variable="nbre_captages_prioritaires",date=as.Date("2021-01-01")) %>% 
  rename(CodeZone=insee_dep,valeur=nbre_captages_prioritaires)

intersection_captage_prioritaire_region<-st_intersection(station_captage_prioritaire,st_buffer(n_region_exp_r52,0))
mapview(intersection_captage_prioritaire_region,zcol=c("insee_reg"),legend=F)+mapview(n_region_exp_r52,alpha.regions=0)
intersection_captage_prioritaire_region_2<-intersection_captage_prioritaire_region %>% 
  st_drop_geometry() %>%
  mutate(nbre_captages_prioritaires=n()) %>%
  select(insee_reg,nbre_captages_prioritaires)%>% 
  unique() %>% 
  mutate(TypeZone="Régions",variable="nbre_captages_prioritaires",date=as.Date("2021-01-01")) %>% 
  rename(CodeZone=insee_reg,valeur=nbre_captages_prioritaires)

intersection_captage_prioritaire_territoire<-bind_rows(intersection_captage_prioritaire_commune_2,
                                                   intersection_captage_prioritaire_epci_2,
                                                   intersection_captage_prioritaire_departement_2,
                                                   intersection_captage_prioritaire_region_2)
rm(intersection_captage_prioritaire_commune_2,
   intersection_captage_prioritaire_epci_2,
   intersection_captage_prioritaire_departement_2,
   intersection_captage_prioritaire_region_2)


# intersection captages prioritaires bénéficiant d'un plan d'actions et territoires -----------
plan_action_commune<-intersection_captage_prioritaire_commune %>% 
  st_drop_geometry() %>% 
  filter(plan_action=="oui") %>% 
  group_by(insee_com) %>% 
  mutate(nbre_captages_prioritaires_avec_plan_actions=n()) %>% 
  ungroup() %>%
  select(insee_com,nbre_captages_prioritaires_avec_plan_actions) %>% 
  unique() %>% 
  right_join(n_commune_exp_r52 %>% 
               select(insee_com) %>%
               st_drop_geometry()) %>% 
  mutate(nbre_captages_prioritaires_avec_plan_actions=ifelse(is.na(nbre_captages_prioritaires_avec_plan_actions),0,nbre_captages_prioritaires_avec_plan_actions)) %>% 
  mutate(TypeZone="Communes",variable="nbre_captages_prioritaires_avec_plan_actions",date=as.Date("2021-01-01")) %>% 
  rename(CodeZone=insee_com,valeur=nbre_captages_prioritaires_avec_plan_actions)

plan_action_epci<-intersection_captage_prioritaire_epci %>% 
  st_drop_geometry() %>% 
  filter(plan_action=="oui") %>% 
  group_by(siren_epci) %>% 
  mutate(nbre_captages_prioritaires_avec_plan_actions=n()) %>% 
  ungroup() %>%
  select(siren_epci,nbre_captages_prioritaires_avec_plan_actions) %>% 
  unique() %>% 
  right_join(n_epci_zsup_r52 %>% 
               select(siren_epci) %>%
               st_drop_geometry()) %>% 
  mutate(nbre_captages_prioritaires_avec_plan_actions=ifelse(is.na(nbre_captages_prioritaires_avec_plan_actions),0,nbre_captages_prioritaires_avec_plan_actions)) %>% 
  mutate(TypeZone="Epci",variable="nbre_captages_prioritaires_avec_plan_actions",date=as.Date("2021-01-01")) %>% 
  rename(CodeZone=siren_epci,valeur=nbre_captages_prioritaires_avec_plan_actions)

plan_action_departement<-intersection_captage_prioritaire_departement %>% 
  st_drop_geometry() %>% 
  filter(plan_action=="oui") %>% 
  group_by(insee_dep) %>% 
  mutate(nbre_captages_prioritaires_avec_plan_actions=n()) %>% 
  ungroup() %>%
  select(insee_dep,nbre_captages_prioritaires_avec_plan_actions) %>% 
  unique() %>% 
  right_join(n_departement_exp_r52 %>% 
               select(insee_dep) %>%
               st_drop_geometry()) %>% 
  mutate(nbre_captages_prioritaires_avec_plan_actions=ifelse(is.na(nbre_captages_prioritaires_avec_plan_actions),0,nbre_captages_prioritaires_avec_plan_actions)) %>% 
  mutate(TypeZone="Départements",variable="nbre_captages_prioritaires_avec_plan_actions",date=as.Date("2021-01-01")) %>% 
  rename(CodeZone=insee_dep,valeur=nbre_captages_prioritaires_avec_plan_actions)

plan_action_region<-intersection_captage_prioritaire_region %>% 
  st_drop_geometry() %>% 
  filter(plan_action=="oui") %>%
  mutate(nbre_captages_prioritaires_avec_plan_actions=n()) %>%
  select(insee_reg,nbre_captages_prioritaires_avec_plan_actions)%>% 
  unique() %>% 
  mutate(TypeZone="Régions",variable="nbre_captages_prioritaires_avec_plan_actions",date=as.Date("2021-01-01")) %>% 
  rename(CodeZone=insee_reg,valeur=nbre_captages_prioritaires_avec_plan_actions)

plan_action_territoire<-bind_rows(plan_action_commune,
                                  plan_action_epci,
                                  plan_action_departement,
                                  plan_action_region)
rm(plan_action_commune,
   plan_action_epci,
   plan_action_departement,
   plan_action_region)


# protection des captages --------------
aac_zpaac<-st_read(dsn_production,query = "SELECT * FROM srnp_dema_si_eau.aac_zpaac")
aac_zpaac_com<-st_read(dsn_production,query = "SELECT * FROM srnp_dema_si_eau.aac_zpaac_com")

ppe <-readOGR(dsn = "extdata/couches_ppe",
              layer = "PPE"
              # encoding = ,
              # use_iconv = 
              ) 
ppe<-st_as_sf(ppe)
ppe_union<-summarise(st_buffer(ppe,0),do_union = TRUE)

ppi_ppr<-readOGR(dsn = "extdata/couches_ppi_ppr",layer = "PPIR_VF") 
ppi_ppr<-st_as_sf(ppi_ppr)
ppi_ppr_union<-summarise(st_buffer(ppi_ppr,0),do_union = TRUE)

protection_captage<-st_union(st_buffer(ppe_union,0),st_buffer(ppi_ppr_union,0)) 


# intersections captages et protections (ppe ou ppi_ppr) ---------
captage_ppe<-st_intersection(ppe_union,captage_eau_potable_ars)
captage_ppi_ppr<-st_intersection(ppi_ppr_union,captage_eau_potable_ars)
captage_protege<-st_intersection(protection_captage,captage_eau_potable_ars)


# intersection captages protégés et territoires ------
intersection_captage_protege_commune<-st_intersection(captage_protege,st_buffer(n_commune_exp_r52,0))
# mapview(intersection_captage_protection_commune,zcol=c("insee_com"),legend=F)+mapview(n_commune_exp_r52,alpha.regions=0)
intersection_captage_protege_commune_2<-intersection_captage_protege_commune %>% 
  st_drop_geometry() %>% 
  group_by(insee_com) %>% 
  mutate(nbre_captages_proteges=n()) %>% 
  ungroup() %>%
  select(insee_com,nbre_captages_proteges) %>% 
  unique() %>% 
  right_join(n_commune_exp_r52 %>% 
               select(insee_com) %>%
               st_drop_geometry()) %>% 
  mutate(nbre_captages_proteges=ifelse(is.na(nbre_captages_proteges),0,nbre_captages_proteges)) %>% 
  mutate(TypeZone="Communes",variable="nbre_captages_proteges",date=as.Date("2021-01-01")) %>% 
  rename(CodeZone=insee_com,valeur=nbre_captages_proteges)

intersection_captage_protege_epci<-st_intersection(captage_protege,st_buffer(n_epci_zsup_r52,0))
# mapview(intersection_captage_protection_epci,zcol=c("siren_epci"),legend=F)+mapview(n_epci_zsup_r52,alpha.regions=0)
intersection_captage_protege_epci_2<-intersection_captage_protege_epci %>% 
  st_drop_geometry() %>% 
  group_by(siren_epci) %>% 
  mutate(nbre_captages_proteges=n()) %>% 
  ungroup() %>%
  select(siren_epci,nbre_captages_proteges) %>% 
  unique() %>% 
  right_join(n_epci_zsup_r52 %>% 
               select(siren_epci) %>%
               st_drop_geometry()) %>% 
  mutate(nbre_captages_proteges=ifelse(is.na(nbre_captages_proteges),0,nbre_captages_proteges)) %>% 
  mutate(TypeZone="Epci",variable="nbre_captages_proteges",date=as.Date("2021-01-01")) %>% 
  rename(CodeZone=siren_epci,valeur=nbre_captages_proteges)

intersection_captage_protege_departement<-st_intersection(captage_protege,st_buffer(n_departement_exp_r52,0))
# mapview(intersection_captage_protection_departement,zcol=c("insee_dep"),legend=F)+mapview(n_departement_exp_r52,alpha.regions=0)
intersection_captage_protege_departement_2<-intersection_captage_protege_departement %>% 
  st_drop_geometry() %>% 
  group_by(insee_dep) %>% 
  mutate(nbre_captages_proteges=n()) %>% 
  ungroup() %>%
  select(insee_dep,nbre_captages_proteges) %>% 
  unique() %>% 
  right_join(n_departement_exp_r52 %>% 
               select(insee_dep) %>%
               st_drop_geometry()) %>% 
  mutate(nbre_captages_proteges=ifelse(is.na(nbre_captages_proteges),0,nbre_captages_proteges)) %>% 
  mutate(TypeZone="Départements",variable="nbre_captages_proteges",date=as.Date("2021-01-01")) %>% 
  rename(CodeZone=insee_dep,valeur=nbre_captages_proteges)

intersection_captage_protege_region<-st_intersection(captage_protege,st_buffer(n_region_exp_r52,0))
# mapview(intersection_captage_protection_region,zcol=c("insee_reg"),legend=F)+mapview(n_region_exp_r52,alpha.regions=0)
intersection_captage_protege_region_2<-intersection_captage_protege_region %>% 
  st_drop_geometry() %>%
  mutate(nbre_captages_proteges=n()) %>%
  select(insee_reg,nbre_captages_proteges)%>% 
  unique() %>% 
  mutate(TypeZone="Régions",variable="nbre_captages_proteges",date=as.Date("2021-01-01")) %>% 
  rename(CodeZone=insee_reg,valeur=nbre_captages_proteges)

intersection_captage_protege_territoire<-bind_rows(intersection_captage_protege_commune_2,
                                        intersection_captage_protege_epci_2,
                                        intersection_captage_protege_departement_2,
                                        intersection_captage_protege_region_2)
rm(intersection_captage_protege_commune_2,
   intersection_captage_protege_epci_2,
   intersection_captage_protege_departement_2,
   intersection_captage_protege_region_2)


# intersection protection et territoires ----------
intersection_protection_captage_commune<-st_intersection(st_buffer(protection_captage,0),st_buffer(n_commune_exp_r52,0))
mapview(intersection_protection_captage_commune)+mapview(n_commune_exp_r52,alpha.regions=0)
intersection_protection_captage_commune_2<-intersection_protection_captage_commune %>% 
  mutate(surf_protection_captage=st_area(geometry)) %>% 
  st_drop_geometry() %>% 
  group_by(insee_com) %>% 
  mutate(part_protection_captage=round(surf_protection_captage/surf_commune*100,digits = 2)) %>% 
  ungroup() %>% 
  right_join(n_commune_exp_r52 %>% 
               select(insee_com) %>%
               st_drop_geometry()) %>% 
  select(insee_com,part_protection_captage) %>% 
  mutate(part_protection_captage=ifelse(is.na(part_protection_captage),0,part_protection_captage)) %>% 
  mutate(TypeZone="Communes",variable="part_protection_captage",date=as.Date("2021-01-01")) %>% 
  rename(CodeZone=insee_com,valeur=part_protection_captage)

intersection_protection_captage_epci<-st_intersection(st_buffer(protection_captage,0),st_buffer(n_epci_zsup_r52,0))
# mapview(intersection_protection_captage_epci)+mapview(n_epci_zsup_r52,alpha.regions=0)
intersection_protection_captage_epci_2<-intersection_protection_captage_epci %>% 
  mutate(surf_protection_captage=st_area(geometry)) %>% 
  st_drop_geometry() %>% 
  group_by(siren_epci) %>% 
  mutate(part_protection_captage=round(surf_protection_captage/surf_epci*100,digits = 2)) %>% 
  ungroup() %>% 
  right_join(n_epci_zsup_r52 %>% 
               select(siren_epci,nom_epci) %>%
               st_drop_geometry()) %>% 
  select(siren_epci,part_protection_captage) %>% 
  mutate(part_protection_captage=ifelse(is.na(part_protection_captage),0,part_protection_captage)) %>% 
  mutate(TypeZone="Epci",variable="part_protection_captage",date=as.Date("2021-01-01")) %>% 
  rename(CodeZone=siren_epci,valeur=part_protection_captage)

intersection_protection_captage_departement<-st_intersection(st_buffer(protection_captage,0),st_buffer(n_departement_exp_r52,0))
# mapview(intersection_protection_captage_departement)+mapview(n_departement_exp_r52,alpha.regions=0)
intersection_protection_captage_departement_2<-intersection_protection_captage_departement %>% 
  mutate(surf_protection_captage=st_area(geometry)) %>% 
  st_drop_geometry() %>% 
  group_by(insee_dep) %>% 
  mutate(part_protection_captage=round(surf_protection_captage/surf_departement*100,digits = 2)) %>% 
  ungroup() %>% 
  right_join(n_departement_exp_r52 %>% 
               select(insee_dep,nom_dep) %>%
               st_drop_geometry()) %>% 
  select(insee_dep,part_protection_captage) %>% 
  mutate(part_protection_captage=ifelse(is.na(part_protection_captage),0,part_protection_captage)) %>% 
  mutate(TypeZone="Départements",variable="part_protection_captage",date=as.Date("2021-01-01")) %>% 
  rename(CodeZone=insee_dep,valeur=part_protection_captage)

intersection_protection_captage_region<-st_intersection(st_buffer(protection_captage,0),st_buffer(n_region_exp_r52,0))
# mapview(intersection_protection_captage_region)+mapview(n_region_exp_r52,alpha.regions=0)
intersection_protection_captage_region_2<-intersection_protection_captage_region %>% 
  mutate(surf_protection_captage=st_area(geometry)) %>% 
  st_drop_geometry() %>%  
  mutate(part_protection_captage=round(surf_protection_captage/surf_region*100,digits = 2)) %>% 
  select(insee_reg,part_protection_captage) %>% 
  mutate(TypeZone="Régions",variable="part_protection_captage",date=as.Date("2021-01-01")) %>% 
  mutate(part_protection_captage=as.numeric(part_protection_captage)) %>% 
  rename(CodeZone=insee_reg,valeur=part_protection_captage)

intersection_protection_captage_territoire<-bind_rows(intersection_protection_captage_commune_2,
                                                      intersection_protection_captage_epci_2,
                                                      intersection_protection_captage_departement_2,
                                                      intersection_protection_captage_region_2)
rm(intersection_protection_captage_commune_2,
   intersection_protection_captage_epci_2,
   intersection_protection_captage_departement_2,
   intersection_protection_captage_region_2)


# assemblage ------------
indicateur_captage_prioritaire_protection_action<-bind_rows(intersection_captage_protege_territoire,
                                                            intersection_protection_captage_territoire,
                                                            intersection_captage_prioritaire_territoire,
                                                            plan_action_territoire) %>%
  left_join(territoire_pdl) %>% 
  mutate_if(is.character,as.factor) %>% 
  select(TypeZone,CodeZone,Zone,variable,valeur,date)


# carte ------------
# mapview(n_region_exp_r52,alpha.regions=0,legend=F)+
#   mapview(aac_zpaac,col.regions="yellow")+
#   mapview(ppe,col.regions="orange")+
#   mapview(ppi_ppr,col.regions="brown")+
#   mapview(ppe_union,col.regions="pink")+
#   mapview(ppi_ppr_union,col.regions="magenta")+
#   mapview(protection_captage,col.regions="white")+
#   mapview(captage_eau_potable_ars)+
#   mapview(station_captage_prioritaire,col.regions="green",color="black")

  
# sauvegarde -------
save.image(file = "sysdata/explo_indicateur_captage_prioritaire_protection_action.RData")
save(indicateur_captage_prioritaire_protection_action,file = "sysdata/indicateur_captage_prioritaire_protection_action.RData")






# # recherche des captages prioritaires (obsolète, pour mémoire) --------
# n_captage_prioritaire_r52<-st_read(dsn_si_eau,query = "SELECT * FROM stations.n_captage_prioritaire_r52")
# captage_prioritaire_pdl<-right_join(station %>%
#                                           select(code_station),
#                                         n_captage_prioritaire_r52,
#                                         by=c("code_station"="code_captage_referent_sise_eaux"))
# captage_prioritaire_pdl_sans_geom<-captage_prioritaire_pdl %>% st_drop_geometry()
# 
# # copie de T:\geomatique\PROJETS\SRNP_DEMA\CAPTAGES_PRIORITAIRES dans exdata
# captage_prioritaire_sur_t <-readOGR(dsn = "extdata/CAPTAGES_PRIORITAIRES",layer = "captages_prioritaires")
# captage_prioritaire_sur_t<-st_as_sf(captage_prioritaire_sur_t) # 48 obs
# 
# # au vu de la carte, ap riori 3 captages supplémentaires entre captage_prioritaire_pdl (45 obs) et captage_prioritaire_sur_t (48 obs) :
# # BSS.C.19 :	05124X0001/HYD ; NOM_SISEAU :	LA FONTAINE DU SON -SOURCE
# # BSS.C.19 :	03184X0002/P ; NOM_SISEAU :	LES AULNAIS
# # BSS.C.19 :	02498X0001/HY ; NOM_SISEAU : PONT DE COUTERNE
# # tous les 3 en dehors des limites de la région pdl
# 
# # pour un captage commun aux 2 tables a priori :
# # code_captage_referent_bss : 04865X0532/SCE   ; code_station : 049000211
# # BSS.C.19 : 04865X0532/SCE ; NOM_SISEAU :	PRIEURE DE LA MADELEINE
# verif_semi_join<-semi_join(captage_prioritaire_sur_t %>% st_drop_geometry(),
#              captage_prioritaire_pdl %>% st_drop_geometry(),
#              by=c("BSS.C.19"="code_captage_referent_bss")) # 36 obs
# 
# verif_anti_join<-anti_join(captage_prioritaire_sur_t %>% st_drop_geometry(),
#              captage_prioritaire_pdl %>% st_drop_geometry(),
#              by=c("BSS.C.19"="code_captage_referent_bss")) # 12 obs
# verif_anti_join_2<-left_join(verif_anti_join,captage_prioritaire_pdl_sans_geom,by=c("COMMUNE.C."="nom_commune")) # 13 obs
# verif_anti_join_3<-filter(verif_anti_join_2,is.na(code_captage_referent_bss)) # 9 obs pas de code_captage_referent_bss renseigné
# verif_anti_join_4<-filter(verif_anti_join_2,!is.na(code_captage_referent_bss)) %>%
#   select(BSS.C.19,NOM_SISEAU,CLASSEMENT,code_station,code_captage_referent_bss,nom_ouvrage_sise_eaux,liste_grenelle_sdage_2016)
# # 4 obs code_captage_referent_bss différent
# # 3 obs avec des noms d'ouvrages identiques sauf le numéro adjoint
# # 1 obs effectivement sans rapport
# 
# mapview(n_region_exp_r52,alpha.regions=0)+
#   mapview(captage_prioritaire_sur_t,col.regions="red",color="red")+
#   mapview(captage_prioritaire_pdl)
# 
# rm(n_captage_prioritaire_r52,captage_prioritaire_pdl,captage_prioritaire_pdl_sans_geom,
#    captage_prioritaire_sur_t,verif_semi_join,verif_anti_join,verif_anti_join_2,verif_anti_join_3,verif_anti_join_4)



