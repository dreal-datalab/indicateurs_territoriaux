library(dplyr)
library(tidyr)
library(COGiter)

load("sysdata/cogifiee_chargement_sitadel_densite.RData")
 indicateur_sitadel_densite<-data_cogifiee %>%
  spread(variable,valeur) %>%
  mutate(id.densite.col_nr = round((id.nb_lgt.col_nr /id.superf_terrain.col_nr ) *10000,digits = 2),
         id.densite.col_rp = round((id.nb_lgt.col_rp /id.superf_terrain.col_rp ) *10000,digits = 2),
         id.densite.col_rs = round((id.nb_lgt.col_rs /id.superf_terrain.col_rs ) *10000,digits = 2),
         id.densite.col_total = round((id.nb_lgt.total_col	 /id.superf_terrain.total_col ) *10000,digits = 2),
         id.densite.ip_nr = round((id.nb_lgt.ip_nr /id.superf_terrain.ip_nr ) *10000,digits = 2),
         id.densite.ip_rp = round((id.nb_lgt.ip_rp	 /id.superf_terrain.ip_rp ) *10000,digits = 2),
         id.densite.ip_rs = round((id.nb_lgt.ip_rs	 /id.superf_terrain.ip_rs ) *10000,digits = 2),         
         id.densite.ip_total = round((id.nb_lgt.total_ip	 /id.superf_terrain.total_ip ) *10000,digits = 2),
         id.densite.ig_nr = round((id.nb_lgt.ig_nr /id.superf_terrain.ig_nr ) *10000,digits = 2),
         id.densite.ig_rp = round((id.nb_lgt.ig_rp /id.superf_terrain.ig_rp ) *10000,digits = 2),
         id.densite.ig_rs = round((id.nb_lgt.ig_rs /id.superf_terrain.ig_rs ) *10000,digits = 2),
         id.densite.ig_total = round((id.nb_lgt.total_ig	 /id.superf_terrain.total_ig ) *10000,digits = 2),
         id.densite.ind_nr = round((id.nb_lgt.ind_nr /id.superf_terrain.ind_nr ) *10000,digits = 2),
         id.densite.ind_rp = round((id.nb_lgt.ind_rp /id.superf_terrain.ind_rp ) *10000,digits = 2),
         id.densite.ind_rs = round((id.nb_lgt.ind_rs /id.superf_terrain.ind_rs ) *10000,digits = 2),
         id.densite.ind_total = round((id.nb_lgt.total_ind	 /id.superf_terrain.total_ind ) *10000,digits = 2),
         id.densite.total_nr = round((id.nb_lgt.total_nr	 /id.superf_terrain.total_nr ) *10000,digits = 2),
         id.densite.total_rp = round((id.nb_lgt.total_rp	 /id.superf_terrain.total_rp ) *10000,digits = 2),
         id.densite.total_rs = round((id.nb_lgt.total_rs	 /id.superf_terrain.total_rs ) *10000,digits = 2),
         id.densite.total = round((id.nb_lgt.total	 /id.superf_terrain.total ) *10000,digits = 2),
         ) %>%
  select(-c(id.nb_lgt.col_nr:id.superf_terrain.total_rs)) %>%
  gather(variable,valeur,id.densite.col_nr:id.densite.total)%>%
  mutate_if(is.character, as.factor)

indicateur_sitadel_densite$valeur[is.nan(indicateur_sitadel_densite$valeur)]<-NA
rm(data_cogifiee)

save(indicateur_sitadel_densite,
     file = "sysdata/indicateur_sitadel_densite.RData")
