# librairies ------------
library(RPostgreSQL)
library(tidyverse)
library(dbplyr)
library(DBI)
library(sf)
library(units)
library(tmap)
library(mapview)
library(lubridate)

# setwd("T:/datalab/SCTE_INDIC_TER/data-raw")

rm(list = ls())

load("sysdata/urba_explo.RData")

# connexion -----------
drv <- dbDriver("PostgreSQL")
con1 <- dbConnect(drv,
                 dbname="referentiels",
                 host=Sys.getenv("server"),
                 port=Sys.getenv("port"),
                 user="does",
                 password=Sys.getenv("pwd_does"))
postgresqlpqExec(con1,"SET client_encoding = 'windows-1252'")
con2 <- dbConnect(drv,
                 dbname="fichiersfonciers",
                 host=Sys.getenv("server"),
                 port=Sys.getenv("port"),
                 user="does",
                 password=Sys.getenv("pwd_does"))
postgresqlpqExec(con2,"SET client_encoding = 'windows-1252'")
con3 <- dbConnect(drv,
                 dbname="consultation",
                 host=Sys.getenv("server"),
                 port=Sys.getenv("port"),
                 user="does",
                 password=Sys.getenv("pwd_does"))
postgresqlpqExec(con3,"SET client_encoding = 'windows-1252'")


# importation des tables pour exemple du 44 --------
# n_doc_urba_s_r52<-st_read(con3,query="select * from amenagement_urbanisme.n_doc_urba_s_r52") # pas utile
# n_zone_urba_s_r52<-st_read(con3,query="select * from amenagement_urbanisme.n_zone_urba_s_r52") # pas utile
# r_plui_s_r52<-st_read(con3,query="select * from amenagement_urbanisme.r_plui_s_r52") # pas exploitable
r_plu_zonage_s_44<-st_read(con3,query="select * from amenagement_urbanisme.r_plu_zonage_s_44") # 131 communes seulement
n_secteur_cc_s_r52<-st_read(con3,query="select * from amenagement_urbanisme.n_secteur_cc_s_r52") # 153 communes


# indicateur 4-03 : poids des différents types de zones --------------

# PLU ou  POS ---------
# (mais aussi carte communale)

## requête à partir de 44
# d44_plu_zonage<-r_plu_zonage_s_44 %>% 
#   st_buffer(.,0) %>% 
#   mutate(surf = round(st_area(.)/10000,digits = 2)) %>% # en hectares
#   group_by(insee,type) %>% 
#   summarise(surf_type = sum(surf)) %>% 
#   ungroup()
# # st_buffer() pour traiter Error in CPL_geos_union(st_geometry(x), by_feature) : Evaluation error: 
# # TopologyException: Input geom 0 is invalid: Self-intersection at or near 
# # point 329135.08015210362 6703082.5599198565 at 329135.08015210362 6703082.5599198565
# d44_plu_zonage<-st_drop_geometry(d44_plu_zonage)
# d44_plu_zonage$surf_type<-drop_units(d44_plu_zonage$surf_type)
# d44_plu_zonage<-d44_plu_zonage %>%
#   mutate(type=ifelse(is.na(type),"99",type))
# 
# d44_plu_zonage$type<-fct_recode(d44_plu_zonage$type,
#                                 "surface_zonage_type_A" = "A",
#                                 "surface_zonage_type_AUc" = "AUc",
#                                 "surface_zonage_type_AUs" = "AUs",
#                                 "surface_zonage_type_N" = "N",
#                                 "surface_zonage_type_Nh" = "Nh",
#                                 "surface_zonage_type_U" = "U",
#                                 "surface_zonage_type_indetermine" = "99")
# d44_plu_zonage<-d44_plu_zonage %>% 
#   rename(key = type,value = surf_type)
# rm(d44_plu_zonage)

creer_surf_zonage_urba<-function(departement){
  query=paste0("select * from amenagement_urbanisme.r_plu_zonage_s_",departement)
  d_plu_zonage<-st_read(con3,query = query)
  
  d_plu_zonage<-d_plu_zonage %>% 
    st_buffer(.,0) %>% 
    mutate(surf = round(st_area(.)/10000,digits = 2)) %>% # en hectares
    group_by(insee,type) %>% 
    summarise(surf_type = sum(surf)) %>% 
    ungroup()
  
  d_plu_zonage<-st_drop_geometry(d_plu_zonage)
  
  d_plu_zonage$surf_type<-drop_units(d_plu_zonage$surf_type)
 
   d_plu_zonage<-d_plu_zonage %>%
    mutate(type=ifelse(is.na(type),"99",type))
  
  d_plu_zonage$type<-fct_recode(d_plu_zonage$type,"surface_zonage_type_A" = "A",
                                "surface_zonage_type_U" = "U",
                                "surface_zonage_type_AUs" = "AUs",
                                "surface_zonage_type_AUc" = "AUc",
                                "surface_zonage_type_N" = "N",
                                "surface_zonage_type_N" = "Ni",
                                "surface_zonage_type_N" = "Na",
                                "surface_zonage_type_Nh" = "Nh",
                                "surface_zonage_type_Nd" = "Nd",
                                "surface_type_zonage_01" = "01",
                                "surface_type_zonage_02" = "02",
                                "surface_type_zonage_03" = "03",
                                "surface_zonage_type_indetermine" = "00",
                                "surface_zonage_type_indetermine" = "99")
  
  d_plu_zonage<-d_plu_zonage %>% 
    rename(variable = type,valeur = surf_type,depcom = insee) %>% 
    mutate(date=dmy("01-01-2017"),depcom = as.factor(depcom),variable = as.factor(variable)) # définir la date !
  
  return(d_plu_zonage)
}

# # corrections intrégrées par rapport à requête élaborée pour le 44
# d53_plu_zonage<-creer_surf_zonage_urba(53)
# # 10 levels au lieu de  7 : Na, Ni et Nd en sus
# r_plu_zonage_s_53<-st_read(con3,query="select * from amenagement_urbanisme.r_plu_zonage_s_53")
# r_plu_zonage_s_53<-st_drop_geometry(r_plu_zonage_s_53) # 9.091 obs dont 8.840 obs renseignées
# # Ni et Na uniquement dans insee = 53088, libelle n'est pas converti en type
# # type N par comparaison avec r_plu_zonage_44
# d72_plu_zonage<-creer_surf_zonage_urba(72)
# # 10 levels au lieu de 7 : 01, 02, 03 en sus
# d85_plu_zonage<-creer_surf_zonage_urba(85)
# # 11 levels au lieu de 7 : 00, 01, 02, 03 en sus
# rm(r_plu_zonage_s_53,d49_plu_zonage,d53_plu_zonage,d72_plu_zonage,d85_plu_zonage)

# # test de la fonction avec 44
# x<-creer_surf_zonage_urba(44)
# rm(x)

# tous les départements sauf 49
r52_surf_zonage_plu<-map(c(44,53,72,85),~creer_surf_zonage_urba(.x))
r52_surf_zonage_plu<-bind_rows(r52_surf_zonage_plu)

# département 49
# d49_plu_zonage<-creer_surf_zonage_urba(49)
# Warning message: Unknown levels in `f`: A, AUc, AUs, N, Nh, U, 99 
r_plu_zonage_s_49<-st_read(con3,query="select * from amenagement_urbanisme.r_plu_zonage_s_49")
# r_plu_zonage_s_49<-st_drop_geometry(r_plu_zonage_s_49) # 12.443 obs dont 12.443 renseignées
# variables chr suivies par  __truncated__ : st_trim() peut être utilisé, 
# mais en fait mauvaise écriture dans le SGBD : espace considérable à la suite des valeurs des variables chr
# réponse SCTE/DSIT : pas de correction sur SGBD et orientation vers portail GPU
r_plu_zonage_s_49$type<-str_trim(r_plu_zonage_s_49$type)
r_plu_zonage_s_49$insee<-str_trim(r_plu_zonage_s_49$insee)
d49_plu_zonage<-r_plu_zonage_s_49 %>% 
  st_buffer(.,0) %>% 
  mutate(surf = round(st_area(.)/10000,digits = 2)) %>% # en hectares
  group_by(insee,type) %>% 
  summarise(surf_type = sum(surf)) %>% 
  ungroup() %>% 
  st_drop_geometry() %>% 
  mutate(type=ifelse(is.na(type),"99",type))
d49_plu_zonage$surf_type<-drop_units(d49_plu_zonage$surf_type)
d49_plu_zonage$type<-fct_recode(d49_plu_zonage$type,"surface_zonage_type_A" = "A",
                                "surface_zonage_type_U" = "U",
                                "surface_zonage_type_AUs" = "AUs",
                                "surface_zonage_type_AUc" = "AUc",
                                "surface_zonage_type_N" = "N",
                                "surface_zonage_type_N" = "Ni",
                                "surface_zonage_type_N" = "Na",
                                "surface_zonage_type_Nh" = "Nh",
                                "surface_zonage_type_Nd" = "Nd",
                                "surface_type_zonage_01" = "01",
                                "surface_type_zonage_02" = "02",
                                "surface_type_zonage_03" = "03",
                                "surface_zonage_type_indetermine" = "00",
                                "surface_zonage_type_indetermine" = "99",
                                "surface_zonage_type_indetermine" = "") 
# dernière ligne ajoutée après constat 13 levels au lieu de 12 pour variable (cf. doublons ci-après) 
d49_plu_zonage<-d49_plu_zonage %>% 
  rename(variable = type,valeur = surf_type,depcom = insee) %>% 
  mutate(date=dmy("01-01-2017"),depcom = as.factor(depcom),variable = as.factor(variable)) # définir la date !

# tous les départements y compris 49
r52_surf_zonage_plu<-bind_rows(r52_surf_zonage_plu,d49_plu_zonage)
# rm(r_plu_zonage_s_49,d49_plu_zonage)

# carte communale --------------
r52_secteur_carte_communale<-n_secteur_cc_s_r52 %>% 
  st_buffer(.,0) %>% 
  mutate(surf = round(st_area(.)/10000,digits = 2)) %>% # en hectares
  group_by(insee,typesect) %>% 
  summarise(surf_type = sum(surf)) %>% 
  ungroup()
r52_secteur_carte_communale<-st_drop_geometry(r52_secteur_carte_communale)
r52_secteur_carte_communale$surf_type<-drop_units(r52_secteur_carte_communale$surf_type)
r52_secteur_carte_communale$typesect<-fct_recode(r52_secteur_carte_communale$typesect,
                                                 "surface_type_zonage_01" = "01",
                                                 "surface_type_zonage_02" = "02",
                                                 "surface_type_zonage_03" = "03",
                                                 "surface_type_zonage_indetermine" = "99")
r52_secteur_carte_communale<-r52_secteur_carte_communale %>% 
  rename(variable = typesect,valeur = surf_type,depcom = insee) %>% 
  mutate(date=dmy("01-01-2017"),depcom = as.factor(depcom),variable = as.factor(variable)) # définir la date !

## recherche de libellés synthétiques pour n_secteur_cc_s_r52$typesect
# z<-st_drop_geometry(n_secteur_cc_s_r52) %>% 
#   select(libelle,typesect) %>% 
#   group_by(libelle,typesect) %>% 
#   unique() %>% 
#   arrange(typesect) # pas de libellés synthétiques pour 01, 02 et 03

# table récapitulative sans doublons ---------
# entre plu ou  POS (mais aussi carte communale), et carte communale
x<-anti_join(r52_secteur_carte_communale,r52_surf_zonage_plu)
r52_surf_zonage<-bind_rows(r52_surf_zonage_plu,x) %>%
  mutate(depcom = as.factor(depcom),variable = as.factor(variable))
rm(x)
# 13 levels pour variable au lieu de 12 ! j'ai corrigé (cf. r52_surf_zonage_plu)
# y<-r52_surf_zonage_plu %>% filter(variable=="") # 17 obs toutes en 49, encore un pb en 49 !
# z<-r52_secteur_carte_communale %>% filter(variable=="") # 0 obs
# w<-r_plu_zonage_s_49 %>% filter(type=="") # 26 obs où en fait on a libelle = 99, donc surface_type_zonage_indetermine
# rm(y,z,w)

# rm(r52_surf_zonage_plu,r52_secteur_carte_communale)


# indicateur 4-05 : surface des parcelles non bâties dans les zones U ----------

# PLU ou  POS ----------
#  (mais aussi carte communale)

## requête à partir de 44
# d44_plu_zonage_U<-r_plu_zonage_s_44 %>%
#   select(insee,type) %>%
#   filter(type %in% c("U","01","02")) # hypothèse à confirmer pour carte communale !
# 
# d44_2017_pnb10_parcelle<-st_read(con2,query = "select *from ff_d44_2017.d44_2017_pnb10_parcelle")
# d44_2017_pnb10_parcelle<-d44_2017_pnb10_parcelle %>%
#   select(idpar,idcom,dcntpa,geomloc,geompar)
# # d44_2017_pnb10_parcelle<-st_drop_geometry(d44_2017_pnb10_parcelle) # conserve geompar
# 
# n_pci_batiment_2017_044<-st_read(con1,query = "select *from plan_cadastral_informatise.n_pci_batiment_2017_044")
# n_pci_batiment_2017_044_dur<-n_pci_batiment_2017_044 %>%
#   filter(dur_code=="01")
# rm(n_pci_batiment_2017_044)
# 
# # y<-st_intersection(d44_2017_pnb10_parcelle,st_buffer(d44_plu_zonage_U,0))
# # y<-y %>% mutate(surf=st_area(geompar))
# #
# # # z<-st_difference(y,n_pci_batiment_2017_044_dur) # je n'ai pas exactement compris le fonctionnement de st_difference
# # z<-st_intersection(y,n_pci_batiment_2017_044_dur)
# # z2<-z %>% select(idpar) %>% st_drop_geometry()
# # z3<-anti_join(y,z2) %>% st_drop_geometry()
# # z4<-z3 %>%
# #   group_by(idcom) %>%
# #   summarise(surf_non_batie=round(sum(surf)/10000,digits = 2),total_dcntpa=round(sum(dcntpa)/10000,digits = 2)) %>%
# #   ungroup()
# # z4$surf_non_batie<-drop_units(z4$surf_non_batie)
# # z4<-z4 %>% mutate(difference = abs(surf_non_batie - total_dcntpa))
# # maximum<-max(z4$difference) # max = 2.88 (ha)
# # moyenne<-round(mean(z4$difference),digits = 2) # moyenne = 0.12 (ha)
# # minimum<-round(min(z4$difference),digits = 2) # minimum = 0 (ha)
# # mediane<-round(median(z4$difference),digits = 2) # médiane = 0.11 (ha)
# 
# d44_surf_parcelle_non_batie_zone_U<-st_intersection(d44_2017_pnb10_parcelle,st_buffer(d44_plu_zonage_U,0)) %>%
#   mutate(surf=st_area(geompar))
# x<-st_intersection(d44_surf_parcelle_non_batie_zone_U,n_pci_batiment_2017_044_dur) %>%
#   select(idpar) %>%
#   st_drop_geometry()
# d44_surf_parcelle_non_batie_zone_U<-anti_join(d44_surf_parcelle_non_batie_zone_U,x) %>%
#   group_by(idcom) %>%
#   summarise(surf_non_batie.zone_U=round(sum(surf)/10000,digits = 2)) %>%
#   ungroup() %>%
#   st_drop_geometry()
# d44_surf_parcelle_non_batie_zone_U$surf_non_batie.zone_U<-drop_units(d44_surf_parcelle_non_batie_zone_U$surf_non_batie.zone_U)
# rm(x)
# 
# d44_surf_parcelle_non_batie_zone_U<-d44_surf_parcelle_non_batie_zone_U %>%
#   gather(key = "variable",value = "valeur",surf_non_batie.zone_U) %>%
#   rename(depcom = idcom) %>%
#   mutate(date=dmy("01-01-2017"),depcom = as.factor(depcom),variable = as.factor(variable)) # définir la date !


creer_surf_parcelle_non_batie_zone_U<-function(annee,departement){
  query_urba = paste0("select * from amenagement_urbanisme.r_plu_zonage_s_",departement)
  d_plu_zonage<-st_read(con3,query = query_urba)
  query_ff = paste0("select * from fichiersfonciers.ff_d",departement,"_",annee,".d",departement,"_",annee,"_pnb10_parcelle")
  d_pnb10_parcelle<-st_read(con2,query = query_ff)
  query_pci = paste0("select * from plan_cadastral_informatise.n_pci_batiment_",annee,"_0",departement)
  d_pci_batiment<-st_read(con1,query = query_pci)
  
  d_plu_zonage_U<-d_plu_zonage %>% 
    select(insee,type) %>% 
    filter(type %in% c("U","01","02")) # hypothèse à confirmer pour carte communale !
  
  d_pnb10_parcelle<-d_pnb10_parcelle %>% 
    select(idpar,idcom,dcntpa,geomloc,geompar)
 
  d_pci_batiment_dur<-d_pci_batiment %>% 
    filter(dur_code=="01")

  d_surf_parcelle_non_batie_zone_U<-st_intersection(d_pnb10_parcelle,st_buffer(d_plu_zonage_U,0)) %>% 
    mutate(surf=st_area(geompar))
  
  x<-st_intersection(d_surf_parcelle_non_batie_zone_U,d_pci_batiment_dur) %>% 
    select(idpar) %>%
    st_drop_geometry()
  d_surf_parcelle_non_batie_zone_U<-anti_join(d_surf_parcelle_non_batie_zone_U,x) %>% 
    group_by(idcom) %>% 
    summarise(surf_non_batie.zone_U=round(sum(surf)/10000,digits = 2)) %>% 
    ungroup() %>% 
    st_drop_geometry()
  
  d_surf_parcelle_non_batie_zone_U$surf_non_batie.zone_U<-drop_units(d_surf_parcelle_non_batie_zone_U$surf_non_batie.zone_U)
  
  d_surf_parcelle_non_batie_zone_U<-d_surf_parcelle_non_batie_zone_U %>%
    gather(key = "variable",value = "valeur",surf_non_batie.zone_U) %>%
    rename(depcom = idcom) %>% 
    mutate(date=dmy("01-01-2017"),depcom = as.factor(depcom),variable = as.factor(variable)) # définir la date !  
 
  return(d_surf_parcelle_non_batie_zone_U)
}

# # test avec 44
# x<-creer_surf_parcelle_non_batie_zone_U(2017,44) # OK
# rm(x)

# tous les départements sauf 49
r52_surf_parcelle_non_batie_zone_U<-map2_dfr(2017,c(44,53,72,85),~creer_surf_parcelle_non_batie_zone_U(2017,.y))

# cas particulier du 49
r_plu_zonage_s_49<-st_read(con3,query="select * from amenagement_urbanisme.r_plu_zonage_s_49")
r_plu_zonage_s_49$type<-str_trim(r_plu_zonage_s_49$type)
r_plu_zonage_s_49$insee<-str_trim(r_plu_zonage_s_49$insee)
d49_2017_pnb10_parcelle<-st_read(con2,query = "select *from ff_d49_2017.d49_2017_pnb10_parcelle")
n_pci_batiment_2017_049<-st_read(con1,query = "select *from plan_cadastral_informatise.n_pci_batiment_2017_049")

d49_plu_zonage_U<-r_plu_zonage_s_49 %>%
  select(insee,type) %>%
  filter(type %in% c("U","01","02")) # hypothèse à confirmer pour carte communale !
d49_2017_pnb10_parcelle<-d49_2017_pnb10_parcelle %>%
  select(idpar,idcom,dcntpa,geomloc,geompar)
n_pci_batiment_2017_049_dur<-n_pci_batiment_2017_049 %>%
  filter(dur_code=="01")
rm(n_pci_batiment_2017_049)

d49_surf_parcelle_non_batie_zone_U<-st_intersection(d49_2017_pnb10_parcelle,st_buffer(d49_plu_zonage_U,0)) %>%
  mutate(surf=st_area(geompar))
x<-st_intersection(d49_surf_parcelle_non_batie_zone_U,n_pci_batiment_2017_049_dur) %>%
  select(idpar) %>%
  st_drop_geometry()
d49_surf_parcelle_non_batie_zone_U<-anti_join(d49_surf_parcelle_non_batie_zone_U,x) %>%
  group_by(idcom) %>%
  summarise(surf_non_batie.zone_U=round(sum(surf)/10000,digits = 2)) %>%
  ungroup() %>%
  st_drop_geometry()
d49_surf_parcelle_non_batie_zone_U$surf_non_batie.zone_U<-drop_units(d49_surf_parcelle_non_batie_zone_U$surf_non_batie.zone_U)
d49_surf_parcelle_non_batie_zone_U<-d49_surf_parcelle_non_batie_zone_U %>%
  gather(key = "variable",value = "valeur",surf_non_batie.zone_U) %>%
  rename(depcom = idcom) %>%
  mutate(date=dmy("01-01-2017"),depcom = as.factor(depcom),variable = as.factor(variable)) # définir la date !
rm(x) 

# tous les départements y compris 49
r52_surf_parcelle_non_batie_zone_U<-bind_rows(r52_surf_parcelle_non_batie_zone_U,d49_surf_parcelle_non_batie_zone_U)
rm(d49_surf_parcelle_non_batie_zone_U)

# carte communale --------
r52_secteur_U_carte_communale<-n_secteur_cc_s_r52 %>% 
  select(insee,typesect) %>% 
  filter(typesect %in% c("01","02")) # hypothèse à confirmer pour carte communale !

creer_surf_parcelle_non_batie_secteur_U<-function(annee,departement){
  query_ff = paste0("select * from fichiersfonciers.ff_d",departement,"_",annee,".d",departement,"_",annee,"_pnb10_parcelle")
  d_pnb10_parcelle<-st_read(con2,query = query_ff)
  query_pci = paste0("select * from plan_cadastral_informatise.n_pci_batiment_",annee,"_0",departement)
  d_pci_batiment<-st_read(con1,query = query_pci)
  
  d_pnb10_parcelle<-d_pnb10_parcelle %>% 
    select(idpar,idcom,dcntpa,geomloc,geompar)
  
  d_pci_batiment_dur<-d_pci_batiment %>% 
    filter(dur_code=="01")
  
  d_surf_parcelle_non_batie_secteur_U<-st_intersection(d_pnb10_parcelle,
                                                       st_buffer(r52_secteur_U_carte_communale,0)) %>% 
    mutate(surf=st_area(geompar))
  
  x<-st_intersection(d_surf_parcelle_non_batie_secteur_U,d_pci_batiment_dur) %>% 
    select(idpar) %>%
    st_drop_geometry()
  d_surf_parcelle_non_batie_secteur_U<-anti_join(d_surf_parcelle_non_batie_secteur_U,x) %>% 
    group_by(idcom) %>% 
    summarise(surf_non_batie.zone_U=round(sum(surf)/10000,digits = 2)) %>% 
    ungroup() %>% 
    st_drop_geometry()
  
  d_surf_parcelle_non_batie_secteur_U$surf_non_batie.zone_U<-drop_units(d_surf_parcelle_non_batie_secteur_U$surf_non_batie.zone_U)
  
  d_surf_parcelle_non_batie_secteur_U<-d_surf_parcelle_non_batie_secteur_U %>%
    gather(key = "variable",value = "valeur",surf_non_batie.zone_U) %>%
    rename(depcom = idcom) %>% 
    mutate(date=dmy("01-01-2017"),depcom = as.factor(depcom),variable = as.factor(variable)) # définir la date !  
  
  return(d_surf_parcelle_non_batie_secteur_U)
}

r52_surf_parcelle_non_batie_secteur_U<-map2_dfr(2017,c(44,49,53,72,85),~creer_surf_parcelle_non_batie_secteur_U(2017,.y))

# table récapitualtive sans doublons ------
# entre PLU et POS (maisaussi carte communale) et carte communale
x<-anti_join(r52_surf_parcelle_non_batie_secteur_U,r52_surf_parcelle_non_batie_zone_U)
r52_surf_parcelle_non_batie_zone_U_2<-bind_rows(r52_surf_parcelle_non_batie_zone_U,x) %>% 
  mutate(depcom = as.factor(depcom),variable = as.factor(variable))
rm(x)

  
# indicateur 4-07 surface des parcelles non bâties dans les zones AU ------------

# PLU et POS -----------
# (mais aussi carte communale) 

# # requête à partir de 44
# d44_plu_zonage_AU<-r_plu_zonage_s_44 %>% 
#   select(insee,type) %>% 
#   filter(type %in% c("AUs","AUc","01")) # hypothèse à confirmer pour carte communale !
# 
# # parcelle et pci déjà traités pour indicateur 4-05
# 
# d44_surf_parcelle_non_batie_zone_AU<-st_intersection(d44_2017_pnb10_parcelle,st_buffer(d44_plu_zonage_AU,0)) %>%
#   mutate(surf=st_area(geompar))
# x<-st_intersection(d44_surf_parcelle_non_batie_zone_AU,n_pci_batiment_2017_044_dur) %>%
#   select(idpar) %>%
#   st_drop_geometry()
# d44_surf_parcelle_non_batie_zone_AU<-anti_join(d44_surf_parcelle_non_batie_zone_AU,x) %>%
#   group_by(idcom) %>%
#   summarise(surf_non_batie.zone_AU=as.numeric(round(sum(surf)/10000,digits = 2))) %>%
#   ungroup() %>%
#   st_drop_geometry()
# 
# d44_surf_parcelle_non_batie_zone_AU<-d44_surf_parcelle_non_batie_zone_AU %>%
#   gather(key = "variable",value = "valeur",surf_non_batie.zone_AU) %>%
#   rename(depcom = idcom) %>%
#   mutate(date=dmy("01-01-2017"),depcom = as.factor(depcom),variable = as.factor(variable)) # définir la date !
# rm(x)

creer_surf_parcelle_non_batie_zone_AU<-function(annee,departement){
  query_urba = paste0("select * from amenagement_urbanisme.r_plu_zonage_s_",departement)
  d_plu_zonage<-st_read(con3,query = query_urba)
  query_ff = paste0("select * from fichiersfonciers.ff_d",departement,"_",annee,".d",departement,"_",annee,"_pnb10_parcelle")
  d_pnb10_parcelle<-st_read(con2,query = query_ff)
  query_pci = paste0("select * from plan_cadastral_informatise.n_pci_batiment_",annee,"_0",departement)
  d_pci_batiment<-st_read(con1,query = query_pci)
  
  d_plu_zonage_AU<-d_plu_zonage %>% 
    select(insee,type) %>% 
    filter(type %in% c("AUs","AUc","01")) # hypothèse à confirmer pour carte communale !
  
  d_pnb10_parcelle<-d_pnb10_parcelle %>% 
    select(idpar,idcom,dcntpa,geomloc,geompar)
  
  d_pci_batiment_dur<-d_pci_batiment %>% 
    filter(dur_code=="01")
  
  d_surf_parcelle_non_batie_zone_AU<-st_intersection(d_pnb10_parcelle,st_buffer(d_plu_zonage_AU,0)) %>% 
    mutate(surf=st_area(geompar))
  
  x<-st_intersection(d_surf_parcelle_non_batie_zone_AU,d_pci_batiment_dur) %>% 
    select(idpar) %>%
    st_drop_geometry()
  d_surf_parcelle_non_batie_zone_AU<-anti_join(d_surf_parcelle_non_batie_zone_AU,x) %>% 
    group_by(idcom) %>% 
    summarise(surf_non_batie.zone_AU=as.numeric(round(sum(surf)/10000,digits = 2))) %>% 
    ungroup() %>% 
    st_drop_geometry()
  
  d_surf_parcelle_non_batie_zone_AU<-d_surf_parcelle_non_batie_zone_AU %>%
    gather(key = "variable",value = "valeur",surf_non_batie.zone_AU) %>%
    rename(depcom = idcom) %>% 
    mutate(date=dmy("01-01-2017"),depcom = as.factor(depcom),variable = as.factor(variable)) # définir la date !  
  
  return(d_surf_parcelle_non_batie_zone_AU)
}

# # test avec 44
# x<-creer_surf_parcelle_non_batie_zone_AU(2017,44) # OK
# rm(x)

# tous les dépatements sauf 49
r52_surf_parcelle_non_batie_zone_AU<-map2_dfr(2017,c(44,53,72,85),~creer_surf_parcelle_non_batie_zone_AU(2017,.y))

# cas particulier du 49
# # pour mémoire (traité dans indicateur 4-05)
# r_plu_zonage_s_49<-st_read(con3,query="select * from amenagement_urbanisme.r_plu_zonage_s_49")
# r_plu_zonage_s_49$type<-str_trim(r_plu_zonage_s_49$type)
# r_plu_zonage_s_49$insee<-str_trim(r_plu_zonage_s_49$insee)
# d49_2017_pnb10_parcelle<-st_read(con2,query = "select *from ff_d49_2017.d49_2017_pnb10_parcelle")
# n_pci_batiment_2017_049<-st_read(con1,query = "select *from plan_cadastral_informatise.n_pci_batiment_2017_049")
# d49_2017_pnb10_parcelle<-d49_2017_pnb10_parcelle %>%
#   select(idpar,idcom,dcntpa,geomloc,geompar)
# n_pci_batiment_2017_049_dur<-n_pci_batiment_2017_049 %>%
#   filter(dur_code=="01")
# rm(n_pci_batiment_2017_049)

d49_plu_zonage_AU<-r_plu_zonage_s_49 %>% 
  select(insee,type) %>% 
  filter(type %in% c("AUs","AUc","01")) # hypothèse à confirmer pour carte communale !

d49_surf_parcelle_non_batie_zone_AU<-st_intersection(d49_2017_pnb10_parcelle,st_buffer(d49_plu_zonage_AU,0)) %>% 
  mutate(surf=st_area(geompar))
x<-st_intersection(d49_surf_parcelle_non_batie_zone_AU,n_pci_batiment_2017_049_dur) %>% 
  select(idpar) %>%
  st_drop_geometry() 
d49_surf_parcelle_non_batie_zone_AU<-anti_join(d49_surf_parcelle_non_batie_zone_AU,x) %>% 
  group_by(idcom) %>% 
  summarise(surf_non_batie.zone_AU=as.numeric(round(sum(surf)/10000,digits = 2))) %>% 
  ungroup() %>% 
  st_drop_geometry()

d49_surf_parcelle_non_batie_zone_AU<-d49_surf_parcelle_non_batie_zone_AU %>%
  gather(key = "variable",value = "valeur",surf_non_batie.zone_AU) %>%
  rename(depcom = idcom) %>% 
  mutate(date=dmy("01-01-2017"),depcom = as.factor(depcom),variable = as.factor(variable)) # définir la date !
rm(x)

# tous les départements y compris 49
r52_surf_parcelle_non_batie_zone_AU<-bind_rows(r52_surf_parcelle_non_batie_zone_AU,d49_surf_parcelle_non_batie_zone_AU)

# carte communale -----------
r52_secteur_AU_carte_communale<-n_secteur_cc_s_r52 %>% 
  select(insee,typesect) %>% 
  filter(typesect %in% c("01")) # hypothèse à confirmer pour carte communale !

creer_surf_parcelle_non_batie_secteur_AU<-function(annee,departement){
  query_ff = paste0("select * from fichiersfonciers.ff_d",departement,"_",annee,".d",departement,"_",annee,"_pnb10_parcelle")
  d_pnb10_parcelle<-st_read(con2,query = query_ff)
  query_pci = paste0("select * from plan_cadastral_informatise.n_pci_batiment_",annee,"_0",departement)
  d_pci_batiment<-st_read(con1,query = query_pci)
  
  d_pnb10_parcelle<-d_pnb10_parcelle %>% 
    select(idpar,idcom,dcntpa,geomloc,geompar)
  
  d_pci_batiment_dur<-d_pci_batiment %>% 
    filter(dur_code=="01")
  
  d_surf_parcelle_non_batie_secteur_AU<-st_intersection(d_pnb10_parcelle,
                                                       st_buffer(r52_secteur_AU_carte_communale,0)) %>% 
    mutate(surf=st_area(geompar))
  
  x<-st_intersection(d_surf_parcelle_non_batie_secteur_AU,d_pci_batiment_dur) %>% 
    select(idpar) %>%
    st_drop_geometry()
  d_surf_parcelle_non_batie_secteur_AU<-anti_join(d_surf_parcelle_non_batie_secteur_AU,x) %>% 
    group_by(idcom) %>% 
    summarise(surf_non_batie.zone_AU=round(sum(surf)/10000,digits = 2)) %>% 
    ungroup() %>% 
    st_drop_geometry()
  
  d_surf_parcelle_non_batie_secteur_AU$surf_non_batie.zone_AU<-drop_units(d_surf_parcelle_non_batie_secteur_AU$surf_non_batie.zone_AU)
  
  d_surf_parcelle_non_batie_secteur_AU<-d_surf_parcelle_non_batie_secteur_AU %>%
    gather(key = "variable",value = "valeur",surf_non_batie.zone_AU) %>%
    rename(depcom = idcom) %>% 
    mutate(date=dmy("01-01-2017"),depcom = as.factor(depcom),variable = as.factor(variable)) # définir la date !  
  
  return(d_surf_parcelle_non_batie_secteur_AU)
}

r52_surf_parcelle_non_batie_secteur_AU<-map2_dfr(2017,c(44,49,53,72,85),~creer_surf_parcelle_non_batie_secteur_AU(2017,.y))

# table récapitulative sans doublons -----------
# entre PLU et POS (mais aussi carte communale) et carte communale
x<-anti_join(r52_surf_parcelle_non_batie_secteur_AU,r52_surf_parcelle_non_batie_zone_AU)
r52_surf_parcelle_non_batie_zone_AU_2<-bind_rows(r52_surf_parcelle_non_batie_zone_AU,x) %>% 
  mutate(depcom = as.factor(depcom),variable = as.factor(variable))
rm(x)


# sauvegarde ------
save.image(file = "sysdata/urba_explo.RData")
