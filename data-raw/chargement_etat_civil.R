library(tidyverse)
library(readxl)
library(tricky)
library(lubridate)


#données Etat Civil issues du site de l'INSEE
# fichiers :  naissances 2008 à 2017 : https://insee.fr/fr/statistiques/1893255
# décés 2008 à 2017 : https://insee.fr/fr/statistiques/1893253
# sélection des fichiers sur https://insee.fr/fr/statistiques ???


## Paramètres
mil <- 2017


etat_civil_naissances <- read_excel(paste0("extdata/base_naissances_",mil,".xls"),sheet=1, skip=5) %>% 
  set_standard_names() 
etat_civil_deces <- read_excel(paste0("extdata/base_deces_",mil,".xls"),sheet=1, skip=5) %>% 
  set_standard_names() 

etat_civil_naissances <- etat_civil_naissances %>%
  gather(date,valeur,5:ncol(etat_civil_naissances))%>%
  mutate(variable="nb_naissances") %>% 
  select(c(depcom=codgeo,date,variable,valeur))
etat_civil_naissances$date <- str_replace(etat_civil_naissances$date,"naisd","20")
etat_civil_naissances <- etat_civil_naissances %>%
  mutate_if(is.character, as.factor)
  

etat_civil_deces <- etat_civil_deces %>%
  gather(date,valeur,5:ncol(etat_civil_deces))%>%
  mutate(variable="nb_deces") %>% 
  select(c(depcom=codgeo,date,variable,valeur))
etat_civil_deces$date <- str_replace(etat_civil_deces$date,"decesd","20")
etat_civil_deces <- etat_civil_deces

etat_civil<- bind_rows(etat_civil_naissances,etat_civil_deces)%>%
  complete(depcom,date,variable,fill = list(valeur =0)) %>%
  mutate(date=make_date(date,12,31))%>% 
  mutate_if(is.character, as.factor)

rm(etat_civil_deces,etat_civil_naissances,mil)

save(etat_civil,file="sysdata/chargement_etat_civil.RData")
