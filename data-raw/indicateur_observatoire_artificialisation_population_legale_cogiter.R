
library(dplyr)
library(tidyr)
library(COGiter)
library(lubridate)
library(sf)

rm(list = ls())


# chargement des données -------------
load("sysdata/cogifiee_chargement_observatoire_artificialisation.RData")
observatoire_artificialisation<-data_cogifiee
# unique(observatoire_artificialisation$date)
# [1] "2010-01-01" "2011-01-01" "2012-01-01" "2013-01-01" "2014-01-01" "2015-01-01" "2016-01-01" "2017-01-01"

load("sysdata/cogifiee_chargement_population_legale.RData")
population_legale<-data_cogifiee
# unique(population_legale$date)
# [1] "1968-01-01" "1975-01-01" "1982-01-01" "1990-01-01" "1999-01-01" "2006-01-01" "2007-01-01" "2008-01-01" "2009-01-01"
# [10] "2010-01-01" "2011-01-01" "2012-01-01" "2013-01-01" "2014-01-01" "2015-01-01" "2016-01-01" "2017-01-01"

rm(data_cogifiee)

communes<-communes_geo %>%
  st_drop_geometry() %>%
  rename(CodeZone=DEPCOM,SurfaceZone=AREA) %>%
  mutate(TypeZone = "Communes")
departements<-departements_geo %>% 
  st_drop_geometry()%>%
  rename(CodeZone=DEP,SurfaceZone=AREA) %>% 
  mutate(TypeZone = "Départements")
epci<-epci_geo %>%
  st_drop_geometry()%>%
  rename(CodeZone=EPCI,SurfaceZone=AREA) %>%
  mutate(TypeZone = "Epci")
regions<-regions_geo %>%
  st_drop_geometry() %>%
  rename(CodeZone=REG,SurfaceZone=AREA) %>%
  mutate(TypeZone = "Régions")

territoires<-bind_rows(communes,departements,epci,regions) 
  # mutate(SurfaceZone=round(SurfaceZone/10000,digits = 0)) # conversion m² en ha

rm(communes,departements,epci,regions)


# calcul de l'indicateur -----------
population_legale<-population_legale %>%
  mutate_if(is.factor,as.character) %>% 
  spread(variable,valeur) %>% 
  filter(date >= "2006-01-01") %>%
  group_by(TypeZone,CodeZone) %>% 
  mutate(diff_population_legale_5ans = population_municipale - lag(population_municipale,5),
         lag5_population_legale = lag(population_municipale,5)) %>%
  ungroup() %>% 
  filter(!is.na(lag5_population_legale)) %>%
  select(-c(population_municipale,lag5_population_legale))
  # mutate(diff_5ans_population_legale = ifelse(diff_5ans_population_legale<0,NA,diff_5ans_population_legale))

observatoire_artificialisation<-observatoire_artificialisation %>% 
  # mutate(valeur = round(valeur/10000,digits = 2)) %>% # conversion m² en ha
  mutate_if(is.factor,as.character) %>% 
  spread(variable,valeur) %>% 
  select(-c(flux_naf_artificialisation_activite:flux_naf_artificialisation_mixte)) %>% 
  group_by(TypeZone,CodeZone) %>% 
  mutate(lag1 = lag(flux_naf_artificialisation_total,1),
         lag2 = lag(flux_naf_artificialisation_total,2),
         lag3 = lag(flux_naf_artificialisation_total,3),
         lag4 = lag(flux_naf_artificialisation_total,4)
         ) %>%
  mutate(naf_artificialisation_5ans = flux_naf_artificialisation_total+lag1+lag2+lag3+lag4) %>%
  select(-c(flux_naf_artificialisation_total:lag4)) %>% 
  filter(!is.na(naf_artificialisation_5ans)) %>%
  ungroup()

indicateur_observatoire_artificialisation_population_legale_cogiter<-left_join(observatoire_artificialisation,population_legale) %>% 
  mutate(naf_artificialisation_par_hbt_accueilli_5ans = round(naf_artificialisation_5ans/diff_population_legale_5ans,digits = 2)) %>% 
  left_join(territoires) %>%
  mutate(pourcentage_naf_artificialisation_5ans_sur_SurfarceZone = round(naf_artificialisation_5ans/SurfaceZone *100,digits = 1)) %>%
  select(-c(diff_population_legale_5ans,SurfaceZone)) %>%
  gather(key = "variable",value = "valeur",
         naf_artificialisation_5ans,
         naf_artificialisation_par_hbt_accueilli_5ans,
         pourcentage_naf_artificialisation_5ans_sur_SurfarceZone
         # etalement_urbain_5ans
         ) %>% 
  mutate_if(is.character,as.factor)

# indicateur_etalement_urbain_typologie<-left_join(observatoire_artificialisation,population_legale) %>% 
#   mutate(etalement_urbain_5ans = case_when(
#     diff_population_legale_5ans>0 & naf_artificialisation_5ans<0 ~ "La population a augmenté et les surfaces artificialisées ont diminué.",
#     diff_population_legale_5ans>0 & naf_artificialisation_5ans==0 ~ "La population a augmenté et les surfaces artificialisées sont restées stables.",
#     diff_population_legale_5ans>0 & naf_artificialisation_5ans>0 ~ "La population a augmenté et les surfaces artificialisées ont augmenté.",
#     
#     diff_population_legale_5ans==0 & naf_artificialisation_5ans>0 ~ "La population est restée stable et les surfaces artificialisées ont augmenté.",
#     diff_population_legale_5ans==0 & naf_artificialisation_5ans<0 ~ "La population est restée stable et les surfaces artificialisées ont diminué.",
#     diff_population_legale_5ans==0 & naf_artificialisation_5ans==0 ~ "La population est restée stable et les surfaces artificialisées sont restées stables.",
#     
#     diff_population_legale_5ans<0 & naf_artificialisation_5ans<0 ~ "La population a diminué et les surfaces artificialisées ont diminué.",
#     diff_population_legale_5ans<0 & naf_artificialisation_5ans>0 ~ "La population a diminué et les surfaces artificialisées ont augmenté.",
#     diff_population_legale_5ans<0 & naf_artificialisation_5ans==0 ~ "La population a diminué et les surfaces artificialisées sont restées stables.")
#     ) %>%
#   select(-c(naf_artificialisation_5ans,diff_population_legale_5ans)) %>% 
#   gather(key = "variable",value = "valeur",etalement_urbain_5ans) %>% 
#   mutate_if(is.character,as.factor)

rm(observatoire_artificialisation,population_legale,territoires)


# sauvegarde ---------
save.image(file = "sysdata/indicateur_observatoire_artificialisation_population_legale_cogiter.RData")
