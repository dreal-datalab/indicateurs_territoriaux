
library(tidyverse)
library(readxl)

rm(list=ls())

# params
mill = 2019

avancement_plui <- read_excel("extdata/avancement_plui_1nov2019.xlsx",sheet=1)

avancement_plui <- avancement_plui %>%
  mutate(date=make_date(mill,12,31),variable="code_avancement_plui") %>%
  rename(valeur=code_variable) %>%
  select(code_epci,date,variable,valeur)
  
save(avancement_plui,file="sysdata/ref_epci_avancement_plui.RData")
