---
title: "L'indicateur d'étalement urbain"
output: html_document
---

### L'indicateur d'étalement urbain

Celui-ci met en regard le taux de croissance de la population et le taux de croissance de l'artificialisation. L'idée est de dire qu'un territoire connait un étalement urbain si chaque nouvel habitant arrivant sur son territoire consomme en moyenne plus que ceux auparavant installés. 

Le Cerema a mit au point une méthode de calcule détaillée [ici](https://www.cerema.fr/system/files/documents/2017/07/Rapport_final_SOeS_phase_2_v8_cle01f492.pdf).

Cette méthode identifie 8 classes.

4 classes pour qualifier pour les territoires __sans étalement urbain__ : 

- classe 1  : régression des surfaces artificialisées avec gain de population ;
- classe 2a : croissance de la population supérieure ou égale à la croissance des surfaces artificialisées cadastrées ;
- classe 2b : perte de population inférieure ou égale à la régression des surfaces artificialisées ;
- classe 2c : recul des surfaces artificialisées inférieur au recul de la population.

_Seule la classe 2a est représentée sur la région._

4 classes pour qualifier les territoires __avec étalement urbain__ : 

- classe 3 : croissance des surfaces artificialisées supérieure à celle de la population ;
- classe 4 : croissance des surfaces artificialisées moins rapide que celle de la population ;
- classe 5 : croissance des surfaces artificialisées plus rapide que celle de la population ;
- classe 6 : croissance des surfaces artificialisées avec perte de population.