library(COGiter)
library(sf)
library(dplyr)

reg <- regions_geo %>% 
  st_transform(4326) %>% 
  left_join(regions) %>% 
  select(CodeZone=REG,Zone=NCCENR) %>% 
  mutate(TypeZone="Régions") %>% 
  filtrer_cog(reg='52')

dep <- departements_geo %>% 
  st_transform(4326) %>% 
  left_join(departements) %>% 
  select(CodeZone=DEP,Zone=NCCENR) %>% 
  mutate(TypeZone="Départements") %>% 
  filtrer_cog(reg='52') 
  
epc <- epci_geo %>% 
  st_transform(4326) %>% 
  left_join(epci) %>% 
  select(CodeZone=EPCI,Zone=NOM_EPCI) %>% 
  mutate(TypeZone="Epci") %>% 
  filtrer_cog(reg='52') 

com <- communes_geo %>% 
  st_transform(4326) %>% 
  left_join(communes) %>% 
  select(CodeZone=DEPCOM,Zone=NOM_DEPCOM) %>% 
  mutate(TypeZone="Communes") %>% 
  filtrer_cog(reg='52') 

save(com,epc,dep,reg,file="app/data/geo.RData")
