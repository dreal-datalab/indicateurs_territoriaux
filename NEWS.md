## Version de développement

- ajout d'un chiffre clef sur la page d'accueil
- extention des sources disponibles
- typo (fix #40)
- Ajout d'une sélection thématique des indicateurs sur la page d'accueil (fix #16)
- Ajout du rapport d'export pour les pages plh et conso d'espace (en attente de pagedown pour finaliser)
- Mise à jour des données de consommation de gaz et d'électricité (ajout 2018 et 2019)