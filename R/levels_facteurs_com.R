library(COGiter)
library(tidyverse)


reg <- 52

com_reg_et_vois <- filter(communes, grepl(reg, REGIONS_DE_L_EPCI)) %>%
  select(DEPCOM) %>%
  mutate(DEPCOM=as.character(DEPCOM)) %>%
  pull(DEPCOM)

com_reg <- filter(communes, REG==reg) %>%
  select(DEPCOM) %>%
  mutate(DEPCOM=as.character(DEPCOM)) %>%
  pull(DEPCOM)

